class Student {
    constructor(name, surname, group){
        this._name=name; //pun _ pt ca altfel s-ar fi apelat setter-ul la infinit
        this._surname=surname;
        this._group=group;
    }

    set name(value){
        this._name=value;
    }

    get name(){
        return this._name;
    }

    set surname(value){
        this._surname=value
    }

    get surname(){
        return this._surname;
    }

    set group(value){
        this._group=value;
    }

    get group(){
        return this._group;
    }

    doSomething = () =>{
        console.log(this);
    }

    doSomething1 = () =>{
        console.log(this);
        function callMe() {
            console.log(this);
        }
        callMe.bind(this)();
    }

}

class Employee extends Student{
    constructor(name,surname,group,salary){
        super(name,surname,group);
        this.salary = salary;
    }
}


let student = new Student('Gigel', 'Popel', 2000);
let employee = new Employee('Popescu', 'Andrei', 1077,8000);
let employee1 = new Employee('Gigi', 'Behehe', 2001, 5200);
let employee2 = new Employee('Gigi', 'Behehe', 2001, 5500);

employee.doSomething1();
student.doSomething1();

const employees = [];
employees.push(employee);
employees.push(employee1);
employees.push(employee2);

console.log(employees.map(emp => emp.salary));
console.log(employees.filter(emp => emp.salary > 7000));
console.log(employees.reduce((total,emp) => total + emp.salary,0)); //suma elementelor

