
var person={
    name:'Gigi',
    surname: 'Popescu'
}; // new Object() => object literlas
console.log(person);
person.name='John';
person.surname='Doe';
console.log(person);

var person1={};
person1.nume='Jane';
person1.prenume='Doe'
console.log(person1);

//in js nu avem clase si totul reprezinta un obiect
//var window -> var this care initial pointeaza la window si enviroment... care ne ajuta sa ne mutam intre mediile de lucru? 
function Person(name, surname){
    this.name = name;
    this.surname = surname;
}

Person.prototype.getDeailsNormal = function(){
    return this.name + ' ' + this.surname;
}

//pun `` ca sa ii spun ca ma folosesc de interpolare interpolare
Person.prototype.getDetailsInterpolation = function(){
    return `${this.name} ${this.surname}`;
}

var person2 = new Person('Jane', 'Doe');
console.log(person2);
console.log(person2.getDeailsNormal());
console.log(person2.getDetailsInterpolation());

//mostenire
function Student(name, surname, grupa){
    Person.call(this, name, surname);
    this.grupa = grupa;
}

Student.prototype = Object.create(Person.prototype);
//prototipul lui student va fi creat pe baza prototipului lui person ca sa pot sa fac mostenirea

Object.defineProperty(Student.prototype, 'constructor',{
    value: Student,
    enumerable: false,
    writable: true
});

//aici am folosit closure
Student.prototype.studentFactory = function(university){
    var self=this;
    function signIn(){
        // console.log(this);
        // console.log(`${this.name} ${this.surname} signed in to the university ${university}` );
        console.log(self);
        console.log(`${self.name} ${self.surname} signed in to the university ${university}` );
    }

    function graduate(){
        // console.log(this);
        // console.log(`${this.name} ${this.surname}  graduated the university ${university}`)
        console.log(self);
        console.log(`${self.name} ${self.surname}  graduated the university ${university}`)
    }

    //aici se pierde this pentru ca el o sa devina obiectul returnat
    return{
        signIn: signIn,
        graduate: graduate
    }
}

//aici vom folosi binding
Student.prototype.studentFactory2 = function(university){
    
    function signIn(){
        console.log(this);
        console.log(`${this.name} ${this.surname} signed in to the university ${university}` );
    }

    function graduate(){
        console.log(this);
        console.log(`${this.name} ${this.surname}  graduated the university ${university}`)
    }

    //prin binding, aici preia this ul din student
    return{
        signIn: signIn.bind(this),
        graduate: graduate.bind(this)
    }
}

//folosim arrow functions
//nu face binding catre this-ul in care a fost apelat, ci se uita in contextul anterior, adica in student
Student.prototype.studentFactory3 = function(university){
    
    signIn = () => {
        console.log(this);
        console.log(`${this.name} ${this.surname} signed in to the university ${university}` );
    }

    graduate = () => {
        console.log(this);
        console.log(`${this.name} ${this.surname}  graduated the university ${university}`)
    }

    return{
        signIn: signIn,
        graduate: graduate
    }
}


var student=new Student('Gigescu', 'Popescu', 1077);
console.log(Student.prototype.constructor);
console.log(student);
console.log(student.getDeailsNormal());

student.studentFactory('ASE').signIn(); //am pierdut referinta la this; o sa se af: undefined undefined signed in to the university ASE
//=> trebuie sa salvez this intr o variabila intermediara (acum se va af ce trebuie pt ca am salvar in self referinta lui this)

student.studentFactory2('Poli').graduate();
student.studentFactory3('ASE').graduate();

