function Student(name, surname) {
    this.name = name;
    this.surname = surname;
}

console.log(Student.prototype);

Student.prototype.getStudentDetails = function() {
    return this.name + ' ' + this.surname;
}


Student.prototype.factory = function(college){

    function signIn(){
        return `${this.name} ${this.surname} started college ${college}`
    }

    function graduate(){
        return `${this.name} ${this.surname} graduated college ${college}`
    }

    return{
        signIn: signIn,     
        graduate: graduate
    }
}

Student.prototype.factory1 = function(college){
    let self = this;
    function signIn(){
        return `${self.name} ${self.surname} started college ${college}`
    }

    function graduate(){
        return `${self.name} ${self.surname} graduated college ${college}`
    }

    return{
        signIn: signIn,
        graduate: graduate
    }
}

Student.prototype.factory2 = function(college){

    function signIn(){
        return `${this.name} ${this.surname} started college ${college}`
    }

    function graduate(){
        return `${this.name} ${this.surname} graduated college ${college}`
    }

    return{
        signIn: signIn.bind(this),      //prin bind, se priea this-ul din student
        graduate: graduate.bind(this)
    }
}

var student = new Student('Cosmin', 'Cartas');
console.log(student.getStudentDetails());

console.log(student.factory('ASE').signIn()); //undefined undefined started college ASE
//pt ca this ul se pierde deoarece in return, this-ul devine obiectul returnat
//pt a rezolva exista 2 variante: salvam this ul intr-o variabila
//sau facem binding la return

console.log(student.factory1('ASE').signIn());
console.log(student.factory2('ASE').signIn());

//mostenire
function Employee(name, surname, salary){
    Student.call(this,name,surname); //se apeleaza functia student pt initializare
    this.salary=salary;
}

//pt a se putea realiza mostenirea, prototipul lui employee trebuie 
//sa fie construit pe baza prototipului lui Student
Employee.prototype = Object.create(Student.prototype);

const employee1 = new Employee('Cocos', 'Da', 1000);
console.log(Employee.prototype.constructor);


Object.defineProperty(Employee.prototype, 'constructor', {
    value: Employee,
    enumerable: false,
    writable: true
});

console.log(Employee.prototype.constructor);
