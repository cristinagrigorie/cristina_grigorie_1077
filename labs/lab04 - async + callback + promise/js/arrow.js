const func1 = () =>{
    console.log("First arrow funct")
}

func1();

const func2 = () => 2;
console.log(func2());

const func3 = (a,b) => a+b;
console.log(func3(2,4));

const func4 = (a,b) =>{
    return{
        a, b
    }
};

const funct4 = (a, b) => {
    console.log("I'm doing some additional processing");
    return a+b;
}



console.log(func4(1,2));
console.log(funct4(1,2));