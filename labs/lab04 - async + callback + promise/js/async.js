

//https://javascript.info/promise-basics
function authenticate(username, pass){
    return new Promise((resolve,reject) =>{
        const payload={
            data:{},
            error: null
        };

        setTimeout(() =>{
            if(username === 'admin' && pass === 'p@ss'){    //daca astea sunt credentialele
                //populez obiectul payload
                payload.data.authentication_token='SECRET_TOKEN';
                payload.data.expire_time=19999;
                payload.data.roles = 'ADMIN';

                resolve(payload.data); 
            } else{
                payload.error='Unauthorized';
                reject(payload.error);
            }
        }, 1000); //sa se execute dupa 3 secunde
    });
}

function redirectUser(userData, path){
    return new Promise((resolve,reject) =>{
        if(!userData.roles){
            reject('Unauthorized');
        } else{
            if(path.includes('admin')){
                resolve('Wlcome admin!');
            } else{
                resolve('Welcome user!');
            }
        }
    });
}

async function display(){
    const userData = await authenticate('admin', 'p@ss');
    console.log(userData);
}

display();

// (async function() {
//     const userData = await authenticate('admin', 'p@ss');
//     const message = await redirectUser(userData, '/admin/home');
//     alert(message);
// }())


