function closure(){
    var func =null;
    for(var i=0;i<3;i++){
        func = function(){
            console.log(i);
        };
    }
    return func;
}

function closure1() {
    var func = null; 
    for(let i=0; i < 3; i++){
        func = function() {
            console.log(i);
        };
    }
    return func;
}

function func2(a,b){
    return (a || 0) + (b || 0);
}


function func3(a = 0, b = 0) {
    return a + b;
}

console.log(func2(5)); //5
console.log(func2()); //0
console.log(func3()); //0
console.log(func3(1,2)); //3
console.log(func3(5)); //5
console.log(closure().prototype); //constructor f
console.log(closure1().prototype);