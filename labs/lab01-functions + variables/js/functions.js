function basicFunction(){
    console.log('basic function')
}

basicFunction();

function sum(a,b){
    return a+b;
}

console.log(sum(5,6));

// default parameters
function sum2(a,b=5){
    return a+b;
}

console.log(sum2(3));
console.log(sum2(3,7));