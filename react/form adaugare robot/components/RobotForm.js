import React from 'react'

class RobotForm extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            name: '',
            type: '',
            mass: 0
        }
        
        this.handleChange = (event) =>{
               this.setState({
                   [event.target.name] : event.target.value
               }); 
        }
    }
    
    handleClick = (event) =>{
        this.props.onAdd({
            name: this.state.name,
            type: this.state.type,
            mass: this.state.mass
        });
    }
    
    render(){
        return(
            <div>
                <input type="text" placeholder="name" onChange={this.handleChange} id="name" name="name"/>
                <input type="text" placeholder="type" onChange={this.handleChange} id="type" name="type"/>
                <input type="text" placeholder="mass" onChange={this.handleChange} id="mass" name="mass"/>
                <input type="button" value="add" onClick={this.handleClick}/>
            </div>
        );
    }
    
}  

export default RobotForm;