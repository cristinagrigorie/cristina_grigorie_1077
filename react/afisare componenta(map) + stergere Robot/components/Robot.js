import React from 'react'

class Robot extends React.Component{
    
    handleDelete = () =>{
        this.props.onDelete(this.props.item.id);
    }
    
    render(){
        const {item} = this.props;
        return(
            <div>
                Robot has name: {item.name}, type: {item.type}, mass: {item.mass}  
                <input type ="button" value="delete" onClick = {this.handleDelete}/>
            </div>
           
        );
    }
}

export default Robot;