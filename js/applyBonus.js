# Avand urmatoarea functie `function applyBonus(employees, bonus)`, completati urmatoarele taskuri:

- Functia trebuie sa returneze un Promise; (0.5 pts)
- Daca `bonus` nu este numar, functia trebuie sa apeleze `reject` cu `Error` si mesajul `Invalid bonus`; (0.5 pts)
- `employees` este un vector ce contine elemente cu urmatorul format: `{name: string, salary: number}` (Example: [{name: "John Doe", salary: 5000}]). Daca este pasat un vector cu elemente invalide, functia trebuie sa apeleze `reject` cu `Error` si mesajul `Invalid array format`; (0.5 pts)
- Functia trebuie sa apeleze `reject` cu `string` cu valoarea `Bonus too small` daca `bonus` este mai mic de 10% din salariul maxim din `employees` array; (0.5 pts)
- Functia trebuie sa apeleze `resolve` cu un vector ce contine salariile marite pentru fiecare angajat; (0.5 pts)


function applyBonus(employees, bonus){
   return new Promise((resolve,reject) =>{
       if(typeof bonus != 'number'){
           reject(new Error("Invalid bonus"));
       } else{
          var max =  Math.max.apply(Math, employees.map(function(o) { return o.salary; }))
          if(bonus < max*0.1){
              reject('Bonus too small');
          }
           for(var obj of employees){
               if(typeof obj.name != 'string' && typeof obj.salary != 'number'){
                   reject(new Error('Invalid array format'));
               } 
           }
           var vect =[];
           for(var i=0;i<employees.length;i++){
             vect.push({name: employees[i].name, salary: employees[i].salary + bonus})
           }
           resolve(vect);
       }
   })
}

let app = {
    applyBonus: applyBonus,
}

module.exports = app;