function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function
	
	/*
 - funcția distance primește ca parametrii două array-uri
 - fiecare element poate apărea cel mult o dată într-un array; orice apariții suplimentare sunt ignorate 
 - distanța dintre cele 2 array-uri este numărul de elemente diferite dintre ele
 - dacă parametrii nu sunt array-uri se va arunca o excepție ("InvalidType")
*/
	
	if(Array.isArray(first) && Array.isArray(second)){
		if(first.length == 0 && second.length == 0){
			return 0;
		}
		
		let firstSorted = first.sort();
		let secondSorted = second.sort();

		for(var i =1; i<firstSorted.length;i++){
			if(firstSorted[i-1] === firstSorted[i]){
				firstSorted.splice(i,1);
			}
		}
		
		for(var i =1; i<secondSorted.length;i++){
			if(secondSorted[i-1] === secondSorted[i]){
				secondSorted.splice(i,1);
			}
		}
		
		let count = 0;
		for(var i =0; i<firstSorted.length;i++){
			if(!secondSorted.includes(firstSorted[i])){
				count++;
			}
		}
		for(var i =0; i<secondSorted.length;i++){
			if(!firstSorted.includes(secondSorted[i])){
				count++;
			}
		}
		
		return count;
		
	} else{
		throw ({message: 'InvalidType'});
	}
	
}


module.exports.distance = distance