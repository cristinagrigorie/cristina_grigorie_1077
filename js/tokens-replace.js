# Avand urmatoarea functie `function addTokens(input, tokens)` unde:
- `input` este un string ce poate sa contina "...". De exemplu: Subsemnatul ..., dominiciliat in ...;
- `tokens` un vector de tokenuri.
- Functia trebuie sa inlocuiasca toate `...` din `input` cu valorile corespunzatoare din `tokens` sub urmatorul format `${tokenName}`, in ordinea in care exista in vector;


function addTokens(input, tokens){
    if(typeof input != 'string'){
       throw new Error('Invalid input');
    }
    if(input.length <6){
        throw new Error('Input should have at least 6 characters');
    }
    
    for(var obj of tokens){
        if(typeof obj["tokenName"] != 'string'){
            throw Error('Invalid array format');
        }
    }
    
     
    if(input.includes('...')){
        for(var obj of tokens){
            let pos = input.indexOf('.'); 
            let text = input.substring(0,pos);
            return `${text}` + "${" + `${obj["tokenName"]}` + "}";
        }
      
         } else{
             for(var obj of tokens){
                  return input;
             }
      }
    
}

const app = {
    addTokens: addTokens
}

module.exports = app;