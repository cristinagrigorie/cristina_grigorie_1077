class Shape{
	constructor(dimensions){
		this.dimensions = dimensions
	}
	area(){
		// TODO

			throw new Error(`not implemented`);
	
	}
}

// TODO: Square, Circle, Rectangle
class Square extends Shape{
	constructor(dimensions){
		super(dimensions);
	}
	area(){
		return this.dimensions.width*this.dimensions.width;
	}
}

class Circle extends Shape{
	constructor(dimensions){
		super(dimensions);

	}
	area(){
		return Math.PI*(this.dimensions.radius*this.dimensions.radius);
	}
	
}

class Rectangle extends Shape{
	constructor(dimensions){
		super(dimensions);
	}
	area(){
		return this.dimensions.height*this.dimensions.width;
	}
}

const app = {
  Shape: Shape,
  Square : Square,
  Circle : Circle,
  Rectangle : Rectangle
}

module.exports = app