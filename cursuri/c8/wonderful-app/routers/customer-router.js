'use strict'
const express = require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const models = require('../models')

router.post('/sync', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.get('/customers', async (req, res) => {
	try{
	    let customers
	    if (req.query && req.query.filter){
	        customers = await models.Customer.findAll({
	            where : {
	                name : {
	                   [Op.like] : `%${req.query.filter}%` 
	                }
	            }
	        })   
	    }
	    else{
	        customers = await models.Customer.findAll()    
	    }
		res.status(200).json(customers)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.post('/customers', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await models.Customer.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await models.Customer.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}
})

router.get('/customers/:id', async (req, res) => {
	try{
		let customer = await models.Customer.findByPk(req.params.id)
		if (customer){
			res.status(200).json(customer)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.put('/customers/:id', async (req, res) => {
	try{
		let customer = await models.Customer.findByPk(req.params.id)
		if (customer){
			await customer.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

router.delete('/customers/:id', async (req, res) => {
	try{
		let customer = await models.Customer.findByPk(req.params.id)
		if (customer){
			await customer.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

module.exports = router