
# Avand urmatoa aplicatie dezvoltata in NodeJS, sa se completeze metoda de tip `POST` de pe calea `/products` :

- Daca nu exista body pentru cererea http, trebuie sa returnati un JSON cu urmatorul format: `{message: "Body is missing"}`. Codul de raspuns trebuie sa fie: `500`;
- Daca body-ul nu respecta formatul unui produs, trebuie sa returnati un JSON cu urmatorul format: `{message: "Invlid body format"}`. Codul de raspuns trebuie sa fie: `500`;
- Pretul unui produs trebuie sa fie mai mare ca 0.In caz contrar trebuie sa returnati un JSON cu urmatorul format: `{message: "Price should be a positive number"}`. Codul de raspuns trebuie sa fie: `500`; 
- Daca produsul exista deja in vector, trebuie sa returnati un JSON cu urmatorul format: `{message: "Product already exists"}`.Codul de raspuns trebuie: `500`. Unicitatea se face in functie de nume;
- Daca body-ul are formatul corespunzator, produsul trebuie adaugat in vector si sa returnati un JSON cu urmatorul format: `{message: "Created"}`. Codul de raspuns trebuie sa fie: `201`;

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.products = [
    {
        name: "Iphone XS",
        category: "Smartphone",
        price: 5000
    },
    {
        name: "Samsung Galaxy S10",
        category: "Smartphone",
        price: 3000
    },
    {
        name: "Huawei Mate 20 Pro",
        category: "Smartphone",
        price: 3500
    }
];

app.get('/products', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/products', (req, res, next) => {
    
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
         res.status(500).json({message: 'Body is missing'});
    } 
    
    try{
        let product = req.body;
        if(product.name &&  product.category && product.price){
            if(product.price< 0){
                 res.status(500).send({message: 'Price should be a positive number'});
            } else{
                var ok =1;
                    for(var prod of app.locals.products){
                        if(product.name === prod.name){
                           ok=0;
                        }
                    }
                    if(ok == 1){
                        app.locals.products.push(product);
                        res.status(201).send({message: 'Created'});
                    }else{
                         res.status(500).send({message: 'Product already exists'});
                    }
                   
                }
          
        } else{
             res.status(500).send({message: 'Invalid body format'});
        }
        
    }catch(err){
          res.status(400).json({message: 'Bad request'});
    }
  
})

module.exports = app;