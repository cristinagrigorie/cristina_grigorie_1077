
const FIRST_NAME = "Cristina";
const LAST_NAME = "Grigorie";
const GRUPA = "1077";

/**
 * Make the implementation here
 */

function initCaching() {
   var cache = {};

   cache.getCache = function(){
       return cache;
   };

   cache.pageAccessCounter = function(str){
   
    if(str != null && str!=undefined){

        str = str.toLowerCase();
        if(cache.hasOwnProperty(str)){
            cache[str]++;
        }
        else{
            cache[str] = 1;
        }
    }
    else
    {
        if(cache.hasOwnProperty('home')){
            cache.home++;
        }
        else
            cache.home=1;
    }
   };
   
      
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

