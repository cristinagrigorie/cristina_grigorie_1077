
const FIRST_NAME = "Cristina";
const LAST_NAME = "Grigorie";
const GRUPA = "1077";

/**
 * Make the implementation here
 */

function numberParser(x){

    if(x===NaN){
        return NaN;
    }

    if(typeof x == "number" && !isFinite(x)){
        return NaN;
    }

    if(typeof x =="number" && (x < Number.MIN_VALUE || x >= Number.MAX_VALUE)){
        return NaN;
    }

    return parseInt(x);
}
module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

