import React from 'react'

class RobotForm extends React.Component{
    constructor(props){
        super(props);
        
        this.state={
            name: "",
            type: "",
            mass: 0
        }
    
    this.handleChangeRobot = (evt) =>{
        this.setState({
            [evt.target.name] : evt.target.value
        })
    }

} 
    
    render(){
        return (
        <div>
        <input type="text" placeholder="name" id="name" onChange={this.handleChangeRobot} name="name"/>
        <input type ="text" placeholder="type" id="type" onChange = {this.handleChangeRobot} name="type"/>
        <input type ="number" placeholder="mass" id="mass" onChange={this.handleChangeRobot} name ="mass"/> 
        <input type="button"  value="add" onClick={() => this.props.onAdd({
            name: this.state.name,
            type: this.state.type,
            mass: this.state.mass
        }) }/>
        </div>
        );
    }
    
}

 export default RobotForm